class User {
	String sessionID;
	String firstName;
	String lastName;
	String emailAddress;
	String birthDate;
    String password;
    // Database db;

    public User() { // pass in database from the main class
        // create random session ID
        sessionID = rand();
    }
    
    public boolean login(String email, String password) {
        
        // access database tables to populate the account information fields
        Database db; // using SQL commands
        if (password.equals(db.Query("password WHERE email == ", email))) {
            return true;
        }
        return false;
    }

    public void signUp(
        String firstName, 
            String lastName,
	            String emailAddress,
	                String birthDate,
                        String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.birthDate = birthDate;
        this.password = password;
    }
}

class LoggedInUser extends User {
    // each of these lists will hold a reference to the element
    // so that they reflect any changes made to the elements
	List<Story> savedStoriesList;
    List<Comment> commentsList;
    
    public LoggedInUser() {
        super();
    }

    public void saveStory(Story story) {
        savedStoriesList.add(story);
    }

    public void comment(Story story, String content) {
        commentsList.add(story.comment(content, this));
    }

    public void tag(String string, Story story) {
        story.tag(string, this);
    }

    public void like(Story story) {
        story.like(this);
    }

    public void dislike(Story story) {
        story.dislike(this);
    }
}

class Journalist extends User {
    // each of these lists will hold a reference to the element
    // so that they reflect any changes made to the elements
    List<Story> publishedStories;
    
    public Journalist() {
        super();
    }

    public void postStory(Story story) {
        publishedStories.add(story);
    }

    public boolean removeStory(Story story) {
        if (publishedStories.contains(story)) {
            publishedStories.remove(story);
            return true;
        }
        else {
            return false;
        }
    }

    public boolean editStory(Story story, Story edited) {
        if (removeStory(story) == true) {
            postStory(edited);
            return true;
        }
        else {
            return false;
        }
    }
}

enum rank {
    SUPERADMIN, ADMIN, USER
};

class Admin extends User {
    enumBody rank;

    public Admin(enumBody rank) {
        super();
        this.rank = rank;
    }

    public void editElement(Element element, String edited) {
        element.content = edited;
    }
}

class Page {
    // each of these lists will hold a reference to the element
    // so that they reflect any changes made to the elements
    String html;
    List<Element> elements;
}

class Element {
	String content;
    String timestamp;

    public Element(String content) {
        this.content = content;
        timestamp = time.now();
    }
}

class Story extends Element {
    // each of these lists will hold a reference to the element
    // so that they reflect any changes made to the elements
    User writtenBy;
	String title;
    String storyType;
    List<User> likes;
    List<User> dislikes;
    List<Element> elements; // list of tags, comments, ads, and substories
        // List<Tag> tags;
        // List<Comment> comments;
        // List<Story> substories;

    public Story(
        String content, 
            String title, 
                Strihg storyType,
                    List<Story> substories) {
        super(content);
        this.title = title;
        this.storyType = storyType;
        this.substories = substories;

        // initialize lists
        List<Tag> tags = new List<>();
        List<User> likes = new List<>();
        List<User> dislikes = new List<>();
        List<Comment> comments = new List<>();
    }

    public void like(User user) {
        likes.add(user);
    }

    public void dislike(User user) {
        dislikes.add(user);
    }

    public void tag(String content, User user) {
        Tag tag = new Tag(content, this, user);
        tags.add(tag);
    }

    public Comment comment(String content, User user) {
        Comment comment = new Comment(content, this, user);
        comments.add(comment);
        return comment;
    }

    public List<Story> getSubstories() {
        return substories;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public int numLikes() {
        return likes.size;
    }

    public int numDislikes() {
        return dislikes.size;
    }
}

class Comment extends Element {
    // each of these lists will hold a reference to the element
    // so that they reflect any changes made to the elements
    Element referencedElement; // story or forum
    User postedBy;
    List<User> likes;
    List<User> dislikes;

    public Comment(String content, Element element, User user) {
        super(content);
        referencedElement = element;
        postedBy = user;

        List<User> likes = new List<>();
        List<User> dislikes = new List<>();
    }
}

class Ad extends Element {
	String imageHash;
    Metadata metadata;
    
    public Ad(String content, String hash, String data) {
        super(content);
        imageHash = hash;
        metadata = data;
    }

    public void redirect() {
        // redirect the user to an external link
        //metadata.link;
    }
}

class Tag extends Element {
    // stores the reference to the element,
    // to reflect any changes
    Element referencedElement;
    User taggedBy;

    public Tag(String content, Element ref, User user) {
        super(content);
        this.referencedElement = ref;
        this.taggedBy = user;
    }
}

class Forum extends Element {
    // each of these lists will hold a reference to the element
    // so that they reflect any changes made to the elements
    List<Comment> comments;

    public Forum(String content) {
        super(content);
    }
}
